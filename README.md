# README #
# AUDIO ALIGN - alfa #

Questo è un progetto eclipse per un tool scritto in Java per l'allineamento di due tracce audio, proventienti da due file audio o video. L'allineamento viene cercato tramite cross correlazione fra un canale audio di ciascun file, viene scelto il primo canale audio disponibile per tracce audio a 2 o più canali.

Viene fornito come risultato in output lo scostamento del secondo file rispetto al primo in termini di numero campioni ("shift") e di tempo ("delay"). Lo scostamento è positivo se la seconda traccia audio è in ritardo rispetto al primo, negativo se in anticipo e nullo se le tracce risultano già allineate.

## METODOLOGIA ##
Le tracce audio vengono caricate in formato pcm floating point a 64bit alla frequenza di campionamento massima fra i due file indipendentemente dai formati audio dei file stessi. Viene letto solo il primo canale audio per ciascun file.
La cross correlazione, come descritta [qui](https://en.wikipedia.org/wiki/Cross-correlation), viene calcolata sfruttando il teorema della convoluzione trasformando i segnali nelle corrispondenti trasformate di Fourier tramite fast Fourier transform. Le trasformate (la prima coniugata) vengono quindi moltiplicate fra loro (element by element) e del prodotto calcolata l'antitrasformata che corrisponde alla cross correlazione dei segnali di partenza.

Il punto di picco della cross correlazione viene interpretato come punto di allineamento fra le due tracce. L'indice di questo punto corrisponde quindi al numero di campioni "shift" fra le due tracce (positivo se la seconda traccia è in ritardo rispetto alla prima). Dividendo lo "shift" per la frequenza di campionamento delle due tracce, ovvero la più alta fra i due file di partenza, si ottiene il corrispondente "delay" in secondi.

## REQUISITI ##
Il tool si basa per la lettura dei dati audio e delle informazioni dei file sul programma [ffmpeg](https://ffmpeg.org/ffmpeg.html) che deve essere installato globalmente nel sistema. Per questo si presuppone che i formati dei file riconosciuti dal programma siano gli stessi supportati da ffmpeg.

Altre librerie utilizzate sono:

* [Apache math](http://commons.apache.org/proper/commons-math/) per il calcolo della FFT e dell'antitrasformata di Fourier.
* [Gson](https://github.com/google/gson) per la lettura e scrittura di dati in formato Json (in previsione di uso futuro principalmente)

Queste librerie sono incluse nella cartella "lib".

## TEST ##
La cartella "testcases" contiene alcuni file usati per il test dell'applicazione. I file riportanti "sintetico" nel nome sono stati generati tramite il programma [Audacity](http://www.audacityteam.org) al solo scopo di verifica, mentre i file "Interno2Samsung.wav" e "Interno2_2Com.wav" rappresentano registrazioni reali.

Nelle prove finora condotte usando questi file in maniera coerente (ovvero file realmente scostati fra loro e non file con audio diverso) il tool si è dimostrato molto preciso. L'unica piccola imprecisione si è rilevata nel confronto fra il file video di test in ritardo di 1 secondo rispetto all'originale audio. In questo caso il delay rilevato è di 1.03s. Nella cartella "build" è possibile trovare l'eseguibile jar.

## USO ##
per lanciare il programma è sufficiente spostarsi nella cartella dell'eseguibile


```
#!bash

cd path/to/build/directory
```

e digitare il comando


```
#!bash

java -jar ./audioalign.jar [file1] [file2]
```

dove [file1] e [file2] indicano gli indirizzi dei file da esaminare

Per ora il programma crea, a scopo di verifica, tre grafici in formato png chiamate "t1.png", "t2.png" e "xcorr.png" che mostrano le forme d'onda delle due tracce audio e la cross correlazione (in valore assoluto). Le dimensioni degli assi sono "numero di campioni" per l'asse delle ascisse e il valore della funzione rappresentata per l'asse delle ordinate.

Per esempio:

![t1.png](https://bytebucket.org/Bikappa/audioalignment/raw/9516b11944b9a8df31f9007ca36a96f0ed1ab0f7/t1.png)

*t1.png*

![t2.png](https://bytebucket.org/Bikappa/audioalignment/raw/9516b11944b9a8df31f9007ca36a96f0ed1ab0f7/t2.png)

*t2.png*

![xcorr.png](https://bytebucket.org/Bikappa/audioalignment/raw/9516b11944b9a8df31f9007ca36a96f0ed1ab0f7/xcorr.png)

*xcorr.png*