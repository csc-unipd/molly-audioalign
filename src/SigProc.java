import java.util.Arrays;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

public class SigProc {
	
	
	public static Complex [] fft(double[] input)
	{
		int minBiggerTwoPower = (int) Math.ceil(Math.log(input.length)/Math.log(2.0));
	
		int newSize = (int) Math.pow(2, minBiggerTwoPower);
		double[] paddedArray = Arrays.copyOf(input, newSize);
		
		FastFourierTransformer transformer = new FastFourierTransformer(DftNormalization.STANDARD);	
		return transformer.transform(paddedArray, TransformType.FORWARD);
		
	}
	
	public static Complex [] ifft(Complex[] input)
	{	
		FastFourierTransformer transformer = new FastFourierTransformer(DftNormalization.STANDARD);	
		return transformer.transform(input, TransformType.INVERSE);
	}
	
	public static Complex [] xcorr(double[] a, double b[])
	{
		
		//create 2 thread for fft calculation, one for each buffer	
		Thread [] fftThreads = new Thread[2];
		
		double [][] buffers = new double[][]{a,b};
		Complex [][] ffTransformations = new Complex[2][];
		
		class FFTTask implements Runnable{

			int bufferIndex;
			public FFTTask(int bufferIndex){
				this.bufferIndex = bufferIndex;
			}
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				ffTransformations[bufferIndex] = fft(buffers[bufferIndex]);
			}
			
		}
		
		for(int i = 0; i<2;++i){
			fftThreads[i] = new Thread(new FFTTask(i));
			
		}
		
		fftThreads[0].start();
		fftThreads[1].start();
		try {
			fftThreads[0].join();
			fftThreads[1].join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		Complex [] fftMultiplication = new Complex[ffTransformations[0].length];
		
		for(int i = 0; i < ffTransformations[0].length; ++i){
			ffTransformations[0][i] = ffTransformations[0][i].conjugate();
			//System.out.print(bFft[i].abs()+"\t");
			fftMultiplication[i] = ffTransformations[0][i].multiply(ffTransformations[1][i]);
		}
		
		//System.out.println();
		return ifft(fftMultiplication);
		
	}

}
