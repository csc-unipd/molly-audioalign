import java.awt.Color;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.math3.complex.Complex;

import com.github.plot.Plot;
import com.github.plot.Plot.AxisFormat;
import com.github.plot.Plot.Data;
import com.google.gson.Gson;

public class AudioAligner {

	public static Logger appLogger = Logger.getLogger("application");
	
	public static Logger getLogger()
	{
		return appLogger;
	}
		
	//Application configuration
	
	public static class Config{
		public String mode = "file";
		public double startTime = -1.0f;
		public double stopTime = -1.0f;
		public String referencePath = null;
		public String targetPath = null;
		public boolean renderPlot = false;
	}
	
	static Config config;
	
	public static String parseArguments(String args[]){
		
		config = new Config();
		
		String invalidArgumentMsg = "Invalid argument: ";
		for(String arg:args){
			if(arg.startsWith("--startTime=")){
				
				String [] pair = arg.split("=");
				if( pair.length !=2 ){
					return invalidArgumentMsg+arg;
				}
				
				try{
					double startTime = Double.parseDouble(pair[1]);
					config.startTime = startTime;
				}catch(Exception e){
					return invalidArgumentMsg+arg;
				}
				
			}else if(arg.startsWith("--stopTime=")){
				String [] pair = arg.split("=");
				if( pair.length !=2 ){
					return invalidArgumentMsg+arg;
				}
				
				try{
					double stopTime = Double.parseDouble(pair[1]);
					config.stopTime = stopTime;
				}catch(Exception e){
					return invalidArgumentMsg+arg;
				}
			} else if(arg.startsWith("--mode=")){
				String [] pair = arg.split("=");
				if( pair.length != 2 ){
					return invalidArgumentMsg+arg;
				}
				
				String modeVal = pair[1];
				if(!(modeVal.equals("file") || modeVal.equals("folder"))){
					return invalidArgumentMsg+arg;
				}
				
				config.mode = modeVal;
				
			}else if(arg.equals("--renderPlot")){
				config.renderPlot = true;
						
			}else{
				//we consider other arguments as path
				String path = arg;
				if(config.referencePath != null 
						&& config.targetPath !=null){
					return "Too many arguments";
				}
				if(config.referencePath == null){
					config.referencePath = path;
				}else{
					config.targetPath = path;
				}			
			}
		}
		
		if(config.referencePath == null || config.targetPath == null){
			return "please specify a two file paths"; 
		}
		
		if(config.mode.equals("file")){
			
			
		} else if(config.mode.equals("folder")){
			config.renderPlot = false;
		}else{
			return "invalid mode";
		}
		return null;
	}
	
	public int alignTracks(AudioTrack t1, AudioTrack t2) {
		
		int bufferSize = Math.max(t1.getAudio().length, t2.getAudio().length);

		double a[] = new double[bufferSize];
		double b[] = new double[bufferSize];

		for (int i = 0; i < bufferSize; ++i) {
			if (i < t1.getAudio().length) {
				a[i] = t1.getAudio()[i];
			}
		}
		for (int i = 0; i < bufferSize; ++i) {
			if (i < t2.getAudio().length) {
				b[i] = t2.getAudio()[i];
			}
		}
		
		if(config.renderPlot){
			renderPlot(a, "reference track", Color.getHSBColor(0.0f, 1.0f, 0.63f));
			renderPlot(b, "target track", Color.getHSBColor(0.66f, 1.0f, 0.63f));
		}
		
		Complex[] xcorr = SigProc.xcorr(a, b);
		double xcorrBuffer[] = new double[xcorr.length];

		int maxIndex = -1;
		double max = -1;
		for (int i = 0; i < xcorr.length; ++i) {
			Complex transformPoint = xcorr[i];
			xcorrBuffer[i] = transformPoint.abs();
			if (transformPoint.abs() > max) {
				max = transformPoint.abs();
				maxIndex = i;
			}
		}

		int shift = maxIndex < xcorr.length / 2 ? maxIndex : -(xcorr.length - maxIndex);
		if(config.renderPlot){
			renderPlot(xcorrBuffer, "cross correlation", Color.getHSBColor(0.33f, 1.0f, 0.63f));
		}
		return shift;
	}
	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException{
		
		String errorMsg = parseArguments(args);
		if(errorMsg != null){
			System.err.println(errorMsg);
			System.exit(1);
		}
		
		errorMsg = checkPaths();
		if(errorMsg != null){
			System.err.println(errorMsg);
			System.exit(1);
		}
		
		if(config.mode.equals("file")){
			//System.out.println("Align files: "+config.referencePath+" "+config.targetPath);
			alignFiles(new String[]{config.referencePath, config.targetPath});
		}else if(config.mode.equals("folder")){
			//System.out.println("Align folders: "+config.referencePath+" "+config.targetPath);
			alignFolders(new String[]{config.referencePath, config.targetPath});
		}else{
			System.exit(1);
		}		
	}
	
	private static void alignFolders(String folderPaths[]){
		String resultFileName = "talign.csv";
		PrintWriter outWriter = null;
		try {
			outWriter = new PrintWriter(resultFileName, "UTF-8");
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Cannot write result file: "+ resultFileName);
		}
			
		File f1 = new File(folderPaths[0]);
		File f2 = new File(folderPaths[1]);
		
		if(!(f1.isDirectory() && f2.isDirectory())){
			System.err.println("not directories");
		}
		
		File [] files1 = f1.listFiles();
		File [] files2 = f2.listFiles();
		
		int countFiles = 0;
		for(File ref: files1){
			//search for same file into dir 2
			if(!ref.getName().matches("^.*\\.(mov|wav)$")){
				//System.out.print(ref.getName()+" doesn't match");
				continue;
			}
			
			for(File target: files2){
				
				if(!target.getName().matches("^.*\\.(mov|wav)$"))
					continue;		
				
				String refName = ref.getName().substring(0, ref.getName().lastIndexOf("."));
				String targetName = target.getName().substring(0, target.getName().lastIndexOf("."));
				
				if(refName.toLowerCase().compareTo(targetName.toLowerCase())==0){
					//ok start processing
					double delay = alignFiles(new String[]{ref.getAbsolutePath(), target.getAbsolutePath()});
					if(outWriter!= null){
						outWriter.println(ref.getName() +";"+target.getName() +";"+delay);
					}
					countFiles++;
					AudioAligner.getLogger().log(Level.INFO, "FILE COUNT: "+countFiles);
				}
				
			}
		}
		
		if(outWriter != null){
			outWriter.flush();
			outWriter.close();
		}
	}
	
	
	private static double alignFiles(String filePaths[]) {		

//		System.out.println("Files list to be synchronized");
		for (String s : filePaths) {
//			System.out.println(s);
		}

		AudioTrack tracks[] = loadTracks(filePaths);

		AudioAligner aa = new AudioAligner();
		int shift = aa.alignTracks(tracks[0], tracks[1]);
		double delay = (double) shift / tracks[0].getSampleRate();
		
		
		String fa =(new File(filePaths[0])).getName();
		String fb =(new File(filePaths[1])).getName();
		
		OutputMessage r = new OutputMessage("result");
		r.add("reference_file", fa);
		r.add("target_file", fb);
		r.add("shift", shift);
		r.add("delay", delay);
		
		Gson g = new Gson();
		System.out.println(g.toJson(r));
		
		return delay;
	}

	private static String checkPaths() {
		
			File reference = new File(config.referencePath);
			File target = new File(config.targetPath);
			
			String mode = config.mode;
			
			boolean referenceOk = mode.equals("file")?reference.isFile():reference.isDirectory();
			boolean targetOk = mode.equals("file")?target.isFile():target.isDirectory();
			if(!(referenceOk && targetOk)){
				return "Paths are not valid in mode: "+config.mode;
			}
			return null;
		
	}

	
	public void renderPlot(double buffer[], String name, Color color) {

		Data plotData = Plot.data();
		for (int i = 0; i < buffer.length; ++i) {
			plotData.xy(i, buffer[i]);
		}

		Plot plot = Plot.plot(Plot.plotOpts()
				.height(400)
				.width(2000)
				.title(name.toUpperCase())
				.grids(buffer.length/20000, 10)
				.labelFont(new Font("Arial",Font.BOLD,14))
				.padding(30)
				).xAxis("samples", Plot.axisOpts().range(0, buffer.length - buffer.length%20000).format(AxisFormat.NUMBER_KGM))
				.yAxis("amplitude", null)
				.series(null, plotData, Plot.seriesOpts()
						.xAxis("samples")
						.yAxis("amplitude")
						.lineWidth(1)
						.color(color));
		
		
		// saving sample_minimal.png
		try {
			plot.save(name, "png");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public static AudioTrack [] loadTracks(String filepaths[])
	{
		
		AudioTrack tracks[] = new AudioTrack[2];
		AudioTrack.AudioFileInfo fileInfos[] = new AudioTrack.AudioFileInfo[2];

		for (int i = 0; i != 2; ++i) {
			fileInfos[i] = AudioTrack.loadAudioFileInfo(filepaths[i]);
			if (fileInfos[i] == null) {
				System.err.println("error loading file: " + filepaths[i]);
				System.exit(1);
			}
			//System.out.println(fileInfos[i]);
		}
		int loadingSampleRate = Math.max(fileInfos[0].getSampleRate(), fileInfos[1].getSampleRate());
	
		class LoadTrackTask implements Runnable
		{
			private int index;

			public LoadTrackTask (int index){this.index = index;}
			
			@Override
			public void run() {				
					tracks[index] = AudioTrack.loadFile(fileInfos[index], loadingSampleRate);

					if (tracks[index] == null) {
						System.err.println("error loading file: " + fileInfos[index].getFilepath());
						System.exit(1);
					}

//					System.out.println("Loaded file: " + fileInfos[index].getFilepath());
//					System.out
//							.println("at sample rate: " + loadingSampleRate + " audio length: " + tracks[index].getAudio().length);
				}
				
			}
		
		Thread loaders[] = new Thread[2];
		for (int i = 0; i != 2; ++i) {
			
			loaders[i] = new Thread(new LoadTrackTask(i));
			loaders[i].start();
		}
		
		try {
			loaders[0].join();
			loaders[1].join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tracks;
	}
	
	private static class OutputMessage{
		Map<String, Object> message;
		
		public OutputMessage(String type){
			message = new LinkedHashMap<>();
			message.put("type", type);
		}
		
		public OutputMessage add(String name, Object value){
			message.put(name, value);
			return this;
		}
	}
}
