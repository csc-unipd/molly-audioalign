import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;

public class AudioTrack {

	// program used to retrieve informations about audio files
	static private String infoProviderProgramName = "ffprobe";

	// program used to retrieve audio data from audio files
	static private String audioDataProviderProgramName = "ffmpeg";

	static String[] infoProviderOptions = new String[] { "-show_streams", null, "-select_streams", "a", 
			"-loglevel", "quiet" };

	static String audioProviderOptions = "-loglevel quiet -map 0:a:0 -ac 1 -c:a pcm_f64be [[samplerate]] -f data -";

	public static class AudioFileInfo {
		protected String filepath;
		protected int sampleRate;
		protected int channels;
		protected int bitsPerSample;
		protected double duration;

		public int getSampleRate() {
			return sampleRate;
		}

		public int getChannels() {
			return channels;
		}

		// public int getBitsPerSample() {
		// return bitsPerSample;
		// }

		public double getDuration() {
			return duration;
		}

		public String getFilepath() {
			return filepath;
		}

		public String toString() {
			StringBuffer b = new StringBuffer();
			b.append("AudioFileInfo\n").append("filePath: " + this.filepath + "\n")
					.append("sampleRate: " + this.sampleRate + "\n").append("channels: " + this.channels + "\n")
					.append("bitsPerSample: " + this.bitsPerSample + "\n").append("duration: " + this.duration + "\n");
			return b.toString();
		}

	}

	private double audio[];
	private int sampleRate;

	public double[] getAudio() {
		return this.audio;
	}

	public int getSampleRate() {
		return this.sampleRate;
	}

	public static AudioFileInfo loadAudioFileInfo(String filepath) {

		try {

			AudioFileInfo info = new AudioFileInfo();
			info.filepath = filepath;
			String command = generateCommand(infoProviderProgramName, filepath, infoProviderOptions, null);
			AudioAligner.getLogger().log(Level.INFO, command);
			Process p = Runtime.getRuntime().exec(command);

			InputStreamReader reader = new InputStreamReader(p.getInputStream());
			char[] buffer = new char[1024];

			int size;
			StringBuffer commandOutput = new StringBuffer();
			while ((size = reader.read(buffer)) != -1) {
				commandOutput.append(buffer, 0, size);
			}

			for (String line : commandOutput.toString().split("(\r)?\n")) {
				boolean isParameterLine = line.indexOf("=") != -1;
				if (isParameterLine) {
					String entry[] = line.split("=", 2);
					String name = entry[0];
					String value = entry[1];

					// AudioAligner.getLogger().log(Level.INFO, "name: " + name
					// + " value: " + value);
					// System.out.println("name: " + name + " value: " + value);

					switch (name) {
					case "sample_rate":
						info.sampleRate = Integer.parseInt(value);
						break;
					case "channels":
						info.channels = Integer.parseInt(value);
						break;
					case "bits_per_sample":
						info.bitsPerSample = Integer.parseInt(value);
						break;
					case "duration":
						info.duration = Double.parseDouble(value);
						break;
					case "duration_ts":
						// info.durationInSamples = new BigInteger(value);
						break;
					default:
						break;
					}
				}
			}

			p.waitFor();
			int exitValue = p.exitValue();
			if (exitValue != 0) {
				return null;
			}
			return info;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public static AudioTrack loadFile(AudioFileInfo fileInfo, int sampleRate) {
		AudioTrack track = new AudioTrack();
		try {

			int samplesReaded = 0;

			String commandOpts = audioProviderOptions;
			if (sampleRate > 0 && sampleRate != fileInfo.sampleRate) {
				commandOpts = commandOpts.replace("[[samplerate]]", "-ar " + sampleRate);
			} else {
				commandOpts = commandOpts.replace("[[samplerate]]", "");
			}

			track.sampleRate = sampleRate;

			AudioAligner.Config cf = AudioAligner.config;
			boolean cut = false;
			double cutStart = 0.0;
			double cutEnd = fileInfo.getDuration();
			if (cf.startTime < cf.stopTime && cf.startTime >= 0.0f && cf.startTime < fileInfo.getDuration()) {

				cutStart = cf.startTime;
				cutEnd = cf.stopTime < fileInfo.getDuration() ? cf.stopTime : fileInfo.getDuration();
			}

			double duration = cutEnd - cutStart;

			String command = audioDataProviderProgramName + " -ss " + cutStart + " -i " + fileInfo.getFilepath()
					+ " -t " + duration + " " + commandOpts;
			//System.out.println(command);
			AudioAligner.getLogger().log(Level.INFO, command);
			Process p = Runtime.getRuntime().exec(command);
			DataInputStream reader = new DataInputStream(p.getInputStream());
			double dataSize = Math.floor(duration * sampleRate);

			int audioLength = (int) Math.ceil(dataSize);

			// AudioAligner.getLogger().log(Level.INFO, fileInfo.toString());
			AudioAligner.getLogger().log(Level.INFO, "allocating array: " + audioLength);

			track.audio = new double[audioLength];

			while (samplesReaded < audioLength) {
				track.audio[samplesReaded] = reader.readDouble();
				samplesReaded++;
			}
			AudioAligner.getLogger().log(Level.INFO, "samplesReaded: " + samplesReaded);

			// System.out.println("samplesReaded: " + samplesReaded);
			// consume all the output from process if something is left
			while (p.getInputStream().read() != -1)
				;

			p.waitFor();
			p.destroy();

			return track;

		} catch (EOFException e) {
			return track;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	private static String generateCommand(String cmdName, String filePath, String[] options, String inputFileOption) {
		String command = cmdName;

		command = String.join(" ", new String[] { command, inputFileOption != null ? inputFileOption : "", filePath });

		for (int i = 0; i < options.length; i = i + 2) {
			String name = options[i];
			String value = options[i + 1];

			command += " " + name;
			if (value != null) {
				command += " " + value;
			}
		}

		return command;
	}
}
